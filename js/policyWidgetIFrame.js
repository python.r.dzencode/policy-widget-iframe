function policyWidgetIFrame(element, options){
            
    const locale = options.locale ? options.locale : '';
    const url = `https://policy.test.universalna.dzencode.com/${locale}`

    const style = options.style;
    let style_string = '';

    if(style){
        // parse and set style string
        Object.keys(style).forEach(key => {
            style_string += `${key}="${style[`${key}`]}" `; // example: += 'width=100%', where width = key, 100% = style[key];
        })
    }

    element.innerHTML = `<iframe src="${url}" ${style_string}></iframe>`
};